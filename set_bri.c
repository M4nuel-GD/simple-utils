#include <stdio.h>
#include <string.h>

int main( int argc, char *argv[] )  
{
   if( argc == 2 )
   {
	int max = 4650;
        int step = 465;
        int brightness;
        int newbrightness;

        FILE * bri = NULL;
	bri = fopen("/sys/class/backlight/intel_backlight/brightness", "r");
        if (bri != NULL){
                fscanf(bri, "%d", &brightness);
        }
        fclose(bri);
	if(strcmp(argv[1], "up") == 0){
        	newbrightness= brightness + step;
        	if (newbrightness < max){

        		bri = fopen("/sys/class/backlight/intel_backlight/brightness", "w");
        		if (bri != NULL){
        		        fprintf(bri, "%d", newbrightness);
        		        fflush(bri);
        		        fclose(bri);
        		}
        	}
	}
	if(strcmp(argv[1], "down") == 0){
                newbrightness= brightness - step;
                if (newbrightness >= 0){
                	bri = fopen("/sys/class/backlight/intel_backlight/brightness", "w");
                	if (bri != NULL){
                	   fprintf(bri, "%d", newbrightness);
                	   fflush(bri);
                	   fclose(bri);
        		}
		}
   	}
   }
}
